


-- phpMyAdmin SQL Dump

-- version 4.8.5

-- https://www.phpmyadmin.net/

--

-- Hôte : 127.0.0.1:3306

-- Généré le :  mar. 18 fév. 2020 à 16:18

-- Version du serveur :  5.7.26

-- Version de PHP :  7.2.18

​

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

SET AUTOCOMMIT = 0;

START TRANSACTION;

SET time_zone = "+00:00";

​

​

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;

/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;

/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;

/*!40101 SET NAMES utf8mb4 */;

​

--

-- Base de données :  `propar`

--

​

DELIMITER $$

--

-- Procédures

--

DROP PROCEDURE IF EXISTS `calculer_CA`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `calculer_CA` ()  begin

        select sum(cout) from operations_backup;

    end$$

​

DELIMITER ;

​

-- --------------------------------------------------------

​

--

-- Structure de la table `client`

--

​

DROP TABLE IF EXISTS `client`;

CREATE TABLE IF NOT EXISTS `client` (

  `numClient` int(11) NOT NULL AUTO_INCREMENT,

  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  `prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  PRIMARY KEY (`numClient`)

) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

​

--

-- Déchargement des données de la table `client`

--

​

INSERT INTO `client` (`numClient`, `nom`, `prenom`) VALUES

(1, 'Botin', 'Gerard'),

(2, 'Jones', 'Jeanne');

​

-- --------------------------------------------------------

​

--

-- Structure de la table `employe`

--

​

DROP TABLE IF EXISTS `employe`;

CREATE TABLE IF NOT EXISTS `employe` (

  `numEmp` int(11) NOT NULL AUTO_INCREMENT,

  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  `prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  `login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  `mdp` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  PRIMARY KEY (`numEmp`)

) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

​

--

-- Déchargement des données de la table `employe`

--

​

INSERT INTO `employe` (`numEmp`, `nom`, `prenom`, `role`, `login`, `mdp`) VALUES

(1, 'Dubois', 'Jean', 'expert', 'admin', '81dc9bdb52d04dc20036dbd8313ed055'),

(8, 'LUCIDARME', 'CYNTHIA', 'apprenti', 'cyn', '81dc9bdb52d04dc20036dbd8313ed055'),

(9, 'BABAR', 'LELEFAN', 'apprenti', 'babar', '81dc9bdb52d04dc20036dbd8313ed055'),

;

​

-- --------------------------------------------------------

​

--

-- Structure de la table `operations`

--

​

DROP TABLE IF EXISTS `operations`;

CREATE TABLE IF NOT EXISTS `operations` (

  `numOp` int(11) NOT NULL AUTO_INCREMENT,

  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  `statut` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  `cout` float NOT NULL,

  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  `numEmp` int(11) NOT NULL,

  `numClient` int(11) NOT NULL,

  PRIMARY KEY (`numOp`),

  KEY `operations_Employe_FK` (`numEmp`),

  KEY `operations_client0_FK` (`numClient`)

) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

​

--

-- Déchargement des données de la table `operations`

--

​

INSERT INTO `operations` (`numOp`, `type`, `statut`, `cout`, `description`, `numEmp`, `numClient`) VALUES

(41, 'grosse', 'en cours', 10000, 'Rajouter menu employee', 1, 1);

​

--

-- Déclencheurs `operations`

--

DROP TRIGGER IF EXISTS `operations_backup`;

DELIMITER $$

CREATE TRIGGER `operations_backup` BEFORE DELETE ON `operations` FOR EACH ROW begin

    insert into operations_backup(

        numOp,

        type,

        statut, 

        cout, 

        description, 

        numEmp, 

        numClient

    )

    values (

       old.numOp,

        old.type,

       'terminee', 

       old.cout, 

       old.description, 

       old.numEmp, 

       old.numClient

    );

    end

$$

DELIMITER ;

​

-- --------------------------------------------------------

​

--

-- Structure de la table `operations_backup`

--

​

DROP TABLE IF EXISTS `operations_backup`;

CREATE TABLE IF NOT EXISTS `operations_backup` (

  `numOp` int(11) NOT NULL AUTO_INCREMENT,

  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,

  `statut` varchar(50) COLLATE utf8_unicode_ci NOT NULL,

  `cout` float NOT NULL,

  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,

  `numEmp` int(11) NOT NULL,

  `numClient` int(11) NOT NULL,

  PRIMARY KEY (`numOp`)

) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

​

--

-- Déchargement des données de la table `operations_backup`

--

​

INSERT INTO `operations_backup` (`numOp`, `type`, `statut`, `cout`, `description`, `numEmp`, `numClient`) VALUES

(20, 'petite', 'terminee', 1000, 'lavage de vitre', 2, 1),

(21, 'moyenne', 'terminee', 2500, 'nettoyage villa', 2, 2),

(8, 'grosse', 'terminee', 10000, 'nettoyage gare', 1, 1),

(11, 'moyenne', 'terminee', 2500, 'nettoyage villa', 1, 1),

(12, 'petite', 'terminee', 1000, 'lavage de vitre', 1, 1),

(22, 'petite', 'terminee', 1000, 'lavage de vitre', 2, 2),

(14, 'petite', 'terminee', 1000, 'lavage de vitre', 1, 1),

(23, 'petite', 'terminee', 1000, 'lavage de vitre', 2, 1),

(16, 'petite', 'terminee', 1000, 'lavage de vitre', 2, 2),

(17, 'moyenne', 'terminee', 2500, 'nettoyage villa', 2, 1),

(18, 'petite', 'terminee', 1000, 'lavage de vitre', 2, 2),

(24, 'moyenne', 'terminee', 2500, 'nettoyage villa', 2, 2),

(25, 'petite', 'terminee', 1000, 'nettoyage gare', 2, 1),

(27, 'moyenne', 'terminee', 2500, 'nettoyage hall immeuble', 1, 2),

(26, 'petite', 'terminee', 1000, 'lavage de vitre', 2, 2),

(28, 'petite', 'terminee', 1000, 'lavage de vitre', 2, 1),

(29, 'moyenne', 'terminee', 2500, 'lavage de trucs', 2, 2),

(30, 'moyenne', 'terminee', 2500, 'nettoyage villa', 2, 1),

(31, 'petite', 'terminee', 1000, 'nettoyage hall immeuble', 2, 2),

(32, 'petite', 'terminee', 1000, 'nettoyage gare', 2, 1),

(33, 'petite', 'terminee', 1000, 'nettoyage gare', 1, 1),

(34, 'grosse', 'terminee', 10000, 'rajoute des caracteres', 1, 1),

(35, 'petite', 'terminee', 1000, 'fdfdf', 1, 1),

(36, 'petite', 'terminee', 1000, 'fdfdf', 1, 1),

(38, 'moyenne', 'terminee', 2500, 'lavage de vitre', 1, 1),

(37, 'moyenne', 'terminee', 2500, 'Lavage du site', 7, 2),

(39, 'petite', 'terminee', 1000, 'dÃ©collage', 1, 1),

(40, 'moyenne', 'terminee', 2500, 'nettoyage gare', 9, 1);

​

--

-- Contraintes pour les tables déchargées

--

​

--

-- Contraintes pour la table `operations`

--

ALTER TABLE `operations`

  ADD CONSTRAINT `operations_Employe_FK` FOREIGN KEY (`numEmp`) REFERENCES `employe` (`numEmp`),

  ADD CONSTRAINT `operations_client0_FK` FOREIGN KEY (`numClient`) REFERENCES `client` (`numClient`);

COMMIT;

​

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;

/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


