<?php
  session_start();
  include "../model/Employe.class.php";
  include "../model/Operations.class.php";
  include "../model/Client.class.php";
  include "../logging/log4php/Logger.php";
  Logger::configure('../logging/loggingconfig.xml'); // Tell log4php to use our configuration file.
  $connexion = Logger::getLogger('connexion'); // Fetch a logger, it will inherit settings from the root logger
 

  if (isset($_POST['valide'])){
      if (isset($_POST['login']) && !empty($_POST['pass'])) {
          // il n'y a pas d'erreur 
          $login = $_POST['login'];
          $mdp = md5($_POST['pass']);
          $_SESSION['login']= $login;
          $_SESSION['pass']= $mdp;
          $test = Employe::verifConn($login, $mdp);

          if ($test == 1) {
              $op = Operations::listOp($login);
              $opFinish = Operations::listOpFinish($login);
              $emp = Employe::listEmp();
              $cA = Employe::chiffreAffaire();
              $client = Client::listClient();
              $_SESSION['op'] = $op;
              $_SESSION['opFinish'] = $opFinish;
              $_SESSION['emp'] = $emp;
              $_SESSION['CA'] = $cA;
              $_SESSION['client']= $client; 
              $access = Employe::access($login, $mdp);
              $_SESSION['access'] = $access;
              if ($access[0]['role'] == "expert") {
                  header('location: ../view/operations/operation_admin.php');
              }else {
                  header('location: ../view/operations/operations.php');
              }

          }else {
              $connexion->info("Identifiants inexistants"); 
              header('location: ../view/connection/connection.php');
          }

      }else {
          header('location: ../view/connection/connection.php');
      }        
  }

  

?>