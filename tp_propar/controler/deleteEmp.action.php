<?php

  session_start();
  include "../model/Employe.class.php";
  include "../model/Operations.class.php";
  include "../logging/log4php/Logger.php";
  Logger::configure('../logging/loggingconfig.xml'); // Tell log4php to use our configuration file.
  $connexion = Logger::getLogger('connexion'); // Fetch a logger, it will inherit settings from the root logger

  if (isset($_POST['update_id'])){
    
    $numEmp= $_POST['update_id'];
    $role = $_POST['role'];
    Employe::updateEmp($numEmp,$role);
    $emp = Employe::listEmp();
    $_SESSION['emp'] = $emp;
  
  }
  

  $id = $_POST['delete_id'];
  
  if (isset($id)){
    Employe::deleteEmp($id);
    $emp = Employe::listEmp();
    $_SESSION['emp'] = $emp;
  }
    
  

  ?>