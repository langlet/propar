<?php
  session_start();
  include "../model/Employe.class.php";
  include "../model/Operations.class.php";
  include "../logging/log4php/Logger.php";
  Logger::configure('../logging/loggingconfig.xml'); // Tell log4php to use our configuration file.
  $connexion = Logger::getLogger('connexion'); // Fetch a logger, it will inherit settings from the root logger

  
    if (isset($_POST['deconnection'])){
      $_SESSION = array();
      session_destroy();
      header('location: ../view/connection/connection.php');
    }

?>