<?php

session_start();
  include "../model/Employe.class.php";
  include "../model/Operations.class.php";
  include "../logging/log4php/Logger.php";
  Logger::configure('../logging/loggingconfig.xml'); // Tell log4php to use our configuration file.
  $connexion = Logger::getLogger('connexion'); // Fetch a logger, it will inherit settings from the root logger

    if (isset($_POST['addEmp'])){
          
      $verif = Employe::verifExist($_POST['loginNewEmp']);
      if ($verif == 0) { 
          $ope = new Employe($_POST['nom'], $_POST['prenom'],$_POST['role'],$_POST['loginNewEmp'],$_POST['passNewEmp']);
          $ope->addEmp();
          $emp = Employe::listEmp();
          $_SESSION['emp'] = $emp;
          header('location: ../view/employee/employee.php');
      }else {
        header('location: ../view/employee/employee.php');
      }
      
      
    }
?>