<?php

session_start();
  include "../model/Employe.class.php";
  include "../model/Operations.class.php";
  include "../logging/log4php/Logger.php";
  Logger::configure('../logging/loggingconfig.xml'); // Tell log4php to use our configuration file.
  $connexion = Logger::getLogger('connexion'); // Fetch a logger, it will inherit settings from the root logger

  
  if (isset($_POST['delete'])){
    $id = $_POST['delete'];
    Operations::deleteOp($id);
    $op = Operations::listOpFinish($_SESSION['login']);
    $_SESSION['opFinish'] = $op;
    
    header('Content-type: application/json');
    echo json_encode($op); 
  }
?>