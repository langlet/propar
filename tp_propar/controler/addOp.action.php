<?php

session_start();
  include "../model/Employe.class.php";
  include "../model/Operations.class.php";
  include "../logging/log4php/Logger.php";
  Logger::configure('../logging/loggingconfig.xml'); // Tell log4php to use our configuration file.
  $connexion = Logger::getLogger('connexion'); // Fetch a logger, it will inherit settings from the root logger

    if (isset($_POST['ajouter'])){
      
      $login = $_SESSION['login'];
      $mdp = $_SESSION['pass'];
      $access = Employe::access($login, $mdp);
      $role = $access[0]['role'];
      $count = Operations::countOp($role);
      if ($count < 5 && $access[0]['role'] == "expert") {
        $ope = new Operations($_POST['type'], "en cours",$_POST['description']);
        $ope->addOp($_POST['employe'], $_POST['client'],$_POST['type']);
        $op = Operations::listOp($login);
        $_SESSION['op'] = $op;
        header('location: ../view/operations/operation_admin.php');

      }elseif ($count < 3 && $access[0]['role'] == "senior") {
        $ope = new Operations($_POST['type'], "en cours",$_POST['description']);
        $ope->addOp($_POST['employe'], $_POST['client'],$_POST['type']);
        $op = Operations::listOp($login);
        $_SESSION['op'] = $op;
        header('location: ../view/operations/operations.php');

      }elseif($count < 1 && $access[0]['role'] == "apprenti") {
        $ope = new Operations($_POST['type'], "en cours",$_POST['description']);
        $ope->addOp($_POST['employe'], $_POST['client'],$_POST['type']);
        $op = Operations::listOp($login);
        $_SESSION['op'] = $op;
        header('location: ../view/operations/operations.php');
      }else {
        if ($access[0]['role'] == "expert") {
          header('location: ../view/operations/operation_admin.php');
        }else {
          header('location: ../view/operations/operations.php');
        }
      }

    }

?>