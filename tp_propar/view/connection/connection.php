<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Propar | Welcome</title>
    <link rel="stylesheet" href="../style/base.css">
    <link rel="stylesheet" href="../style/header.css">
    <link rel="stylesheet" href="../style/form.css">
    <link rel="stylesheet" href="../style/table.css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet"> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  </head>

  <body>
    <header class="header">
      <div class="header_logo">
      </div>
      <div class="header_text">
        <h1 class="header_name">Propar</h1>
        <h2 class="header_baseline">Société de nettoyage</h1>
      </div>
      <div class="header_nav">
        <p class="loginText"> Se connecter</p>
        <a id="btnMenu" class="header_link btnMenu" href="#">
          <button class="btn btn--connect"></button>
        </a>
        <div id="menuNav" class="MenuNav__triangle">
          <form action="../../controler/cnx.action.php" method="post" enctype="multipart/form-data">
            <input class="input menuNav_link" name="login" type="text" placeholder="nom d'utilisateur" required="required"/>
            <input class="input menuNav_link" name="pass" type="password" placeholder="mot de passe" required="required"/>
            <div class="menuNav_link menuNav_link--justifyRight">
              <!--<p><a href="">Mot de passe oublié</a></p>-->
              <a class="header_link"  >
                <button id="connexionBtn" class="btn btn--logIn" type="submit" name="valide"></button>
              </a>
            </div>
          </form>
        </div>
      </div>
    </header>
    <section class="page">
      <div class="page_header">
        <h3 class="page_title">Opérations en cours</h3>
        <input class="input input--search" type="text" id="currentInput" onkeyup="searchFilter('currentOp','currentInput','currentTable')" placeholder="Rechercher une opération..">
      </div>
      <div  class="show_data_op" ></div>
      <div class="page_header">
        <h3 class="page_title">Opérations terminée</h3>
        <input class="input input--search" type="text" id="finishedInput" onkeyup="searchFilter('finishedOp','finishedInput','finishedTable')" placeholder="Rechercher une opération..">
      </div>
      <div class="show_data_op2" ></div>
    </section>

    <script src="../javascript/toggleMenuNav.js"></script>
    <script src="../javascript/searchFilter.js"></script>
    <script src="../javascript/afficheOp.js"></script>
   

  </body>
</html>