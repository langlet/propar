<?php
  session_start(); 
  $op = $_SESSION['op'];
  $opFinish = $_SESSION['opFinish'];
  $access = $_SESSION['access'];
  $client = $_SESSION['client'];
  if (empty($access)) {
    header ('Location: ../connection/connection.php');
  }
  if ($access[0]['role'] == "expert") {
    header ('Location: ../operations/operation_admin.php');
  }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Propar | Operations </title>
    <link rel="stylesheet" href="../style/base.css">
    <link rel="stylesheet" href="../style/header.css">
    <link rel="stylesheet" href="../style/form.css">
    <link rel="stylesheet" href="../style/table.css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
  </head>
  <body>
    <header class="header header--User">
      <div class="header_logo">
      </div>
      <div class="header_text">
        <h1 class="header_name"><?php echo $access[0]['prenom']." ".$access[0]['nom'] ; ?></h1>
        <h2 class="header_baseline">Bienvenue</h2>
      </div>
      <div class="header_nav">
        <form action="../../controler/dcnx.action.php" method="post" enctype="multipart/form-data">
          <a class="header_link"  >
            <button class="btn btn--logOut" name="deconnection"></button>
          </a>
        </form>
      </div>
    </header>
    
    <section class="page">
      <table class="table">

        <tbody>
          <tr class="white_row">
            <td><h3 class="page_title">Nouvelle opération</h3></td>
          </tr>
          <form action="../../controler/addOp.action.php" method="post" enctype="multipart/form-data">
            <tr class="white_row">
              <td>
                <input class="input input--table" type='text' ref='operation' name="description" placeholder='Description' required="required" />
              </td>
              <td>
                <select class="input input--table" ref='type' name="type" required="required">
                  <option value="" hidden>Type</option>
                  <option value="petite">Petite</option>
                  <option value="moyenne">Moyenne</option>
                  <option value="grosse">Grosse</option>
                </select>
              </td>
              <td>
                <select class="input input--table" ref='type' name="client" required="required">
                  <option value="" hidden>Client</option>
                  <?php for ($i=0; $i < count($client); $i++) {  ?>
                    <option value="<?php echo $client[$i]['nom']; ?>"><?php echo $client[$i]['nom']." ".$client[$i]['prenom']; ?></option>
                  <?php } ?>
                </select>
                <!--<input class="input input--table" type='text' ref='client' name="client" placeholder='John Doe' required="required" />-->
              </td>
              <td>
                <input class="input input--table" type='text' ref='assigne' readonly name="employe" value="<?php echo $access[0]['nom']; ?>" />
              </td>
              <td>
                <input class="input input--table" type="text" ref='statut' name="statut" placeholder="en cours" value = "en cours" disabled> 
              </td>
              <td class="text_right">
                <button class="btn btn--addRow" name="ajouter">
                  <svg viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="17.58" cy="17.5" r="17.5" fill="#3E81EA" />
                    <path d="M17.58 12.33v10.34M12.58 17.67h10" stroke="#fff" stroke-width="1.5" stroke-linecap="round" />
                  </svg>
                </button>
              </td>
            </tr>
          </form>
        </tbody>

        <tbody class="currentTable">
          <tr class="white_row">
            <td colspan="5">
              <h3 class="page_title">Opération(s) en cours</h3>
            </td>
            <td>
              <input class="input input--search" type="text" id="currentInput" onkeyup="searchFilter('currentOp','currentInput','currentTable')" placeholder="Rechercher une opération..">
            </td>
          </tr>
          <tr>
            <th> Description </th>
            <th> Type </th>
            <th> Client </th>
            <th> Assigné </th>
            <th> Statut </th>
            <th></th>
          </tr>
          <form>
            <?php for ($i=0; $i < count($op); $i++) {  ?>
              <tr class="currentOp" id="delete<?php echo $op[$i]['numOp'];?>">
                <td class="operation"> <?php echo $op[$i]['description']; ?></td>
                <td> <?php echo $op[$i]['type']; ?></td>
                <td> <?php echo $op[$i]['client']; ?></td>
                <td> <?php echo $op[$i]['assigne']; ?></td>
                <td class="inprogress"> <?php echo $op[$i]['statut']; ?></td>
                <td class="text_right">
                  <button type="button" class="btn btn--complete deleteOp" name="terminer" onclick="deleteOp(<?php echo $op[$i]['numOp'];?>)">
                    <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M19.72 7l-10 10M4.62 11.89l5.1 5.1" stroke-width="2" stroke-linecap="round" />
                    </svg>
                  </button>
                </td>
              </tr>
            <?php } ?>           
          </form>
        </tbody>
        
        <tbody class="finishedTable">
          <tr class="white_row">
            <td colspan="5">
              <h3 class="page_title">Opération(s) terminée(s)</h3>
            </td>
            <td>
              <input class="input input--search" type="text" id="finishedInput" onkeyup="searchFilter('finishedOp','finishedInput', 'finishedTable')" placeholder="Rechercher une opération..">
            </td>
          </tr>
          <tr id="show_data_op_finish" class='finishedOp'></tr>
          <?php for ($i=0; $i < count($opFinish); $i++) {  ?>
            <tr class="finishedOp">
              <td class="operation"> <?php echo $opFinish[$i]['description']; ?></td>
              <td> <?php echo $opFinish[$i]['type']; ?></td>
              <td> <?php echo $opFinish[$i]['client']; ?></td>
              <td> <?php echo $opFinish[$i]['assigne']; ?></td>
              <td class="finished"> <?php echo $opFinish[$i]['statut']; ?></td>
              <td>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </section>
    <script src="../javascript/toggleMenuNav.js"></script>
    <script src="../javascript/searchFilter.js"></script>
    <script src="../javascript/deleteRow.js"></script>
  </body>
</html>