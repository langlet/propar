<?php
  session_start();    
  $emp = $_SESSION['emp'];
  $access = $_SESSION['access'];
  if (empty($access)) {
    header ('Location: ../connection/connection.php');
  }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Propar | Gestion des employés </title>
    <link rel="stylesheet" href="../style/base.css">
    <link rel="stylesheet" href="../style/header.css">
    <link rel="stylesheet" href="../style/form.css">
    <link rel="stylesheet" href="../style/table.css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  </head>

  <body>
    <header class="header header--Admin">
      <div class="header_logo">
      </div>
      <div class="header_text">
        <h1 class="header_name"><?php echo $access[0]['prenom']." ".$access[0]['nom']; ?></h1>
        <h2 class="header_baseline">Bienvenue</h2>
      </div> 

      <div class="header_nav">
        <a id="btnMenu" class="header_link" href="#">
          <button class="btn btn--menu"></button>
        </a>
        <div id="menuNav" class="menuNav">
          <a href="../operations/operation_admin.php" class="menuNav_link" >
            <svg viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M28.36 16.787a4.455 4.455 0 115.916 0l.016.008c.943.494 1.8 1.22 2.521 2.133.722.915 1.294 2 1.685 3.194.128.392.236.793.323 1.2.297 1.397-.89 2.567-2.317 2.567h-9.136c.341.63.63 1.305.862 2.014.176.538.318 1.092.424 1.655.265 1.403-.918 2.57-2.345 2.57H14.045c-1.427 0-2.61-1.167-2.345-2.57.106-.563.248-1.117.424-1.655.232-.709.52-1.384.862-2.014h-9.4c-1.429 0-2.615-1.17-2.317-2.567.087-.407.194-.808.323-1.2.39-1.194.962-2.28 1.684-3.194.722-.913 1.578-1.639 2.521-2.133l.016-.008a4.455 4.455 0 115.917 0l.015.008c.942.494 1.8 1.22 2.52 2.133a10.335 10.335 0 011.775 3.482c.26-.177.527-.338.801-.482l.018-.01a4.998 4.998 0 116.637 0l.017.01c.197.103.39.215.58.337a10.337 10.337 0 011.73-3.336c.722-.914 1.579-1.64 2.522-2.134l.015-.008zm2.467-.722a6.137 6.137 0 01.982 0 2.656 2.656 0 10-.982 0zM8.235 17.88a4.507 4.507 0 001.071 0 4.68 4.68 0 011.602.51c.7.367 1.365.92 1.945 1.655a8.536 8.536 0 011.386 2.637 9.943 9.943 0 01.279 1.05c-.09.097-.179.197-.266.3a.757.757 0 01-.295.058H3.585a.682.682 0 01-.492-.191.266.266 0 01-.064-.102.187.187 0 010-.098 9.94 9.94 0 01.273-1.017 8.536 8.536 0 011.387-2.637c.58-.735 1.245-1.288 1.944-1.655a4.68 4.68 0 011.602-.51zm1.027-1.814a2.656 2.656 0 10-.982 0 6.138 6.138 0 01.982 0zm6.164 9.372l.025-.032c.252-.186.462-.419.612-.684a6.741 6.741 0 011.614-1.2 5.608 5.608 0 011.291-.489 5.013 5.013 0 002.418 0 5.61 5.61 0 011.29.49c.406.213.801.48 1.178.798.184.603.652 1.094 1.257 1.357a9.923 9.923 0 011.409 2.786c.15.462.273.94.366 1.43a.24.24 0 01-.003.122.314.314 0 01-.075.119.69.69 0 01-.5.195H14.046a.69.69 0 01-.498-.195.314.314 0 01-.075-.12.24.24 0 01-.004-.121c.093-.49.215-.968.367-1.43.377-1.154.923-2.18 1.591-3.026zm15.357-7.558a4.68 4.68 0 00-1.602.51c-.699.367-1.365.92-1.945 1.655a8.538 8.538 0 00-1.386 2.637 9.89 9.89 0 00-.23.823c.183.186.36.38.531.585h10.353a.682.682 0 00.492-.191.266.266 0 00.065-.102.187.187 0 000-.098 9.943 9.943 0 00-.274-1.017 8.538 8.538 0 00-1.386-2.637c-.58-.735-1.246-1.288-1.945-1.655a4.68 4.68 0 00-1.602-.51 4.508 4.508 0 01-1.071 0zm-11.724 3.3a6.91 6.91 0 012.236 0 3.199 3.199 0 10-2.236 0z" fill="#333"/></svg>
            Liste des opérations
          </a>
          <a href="../turnover/turnover.php" class="menuNav_link">
            <svg viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M24.5 9.76a.87.87 0 10.12 1.74l5.34-.4-.16-5.34a.87.87 0 00-1.75.05l.08 2.56a14.18 14.18 0 00-20.7 18.48.87.87 0 101.51-.87 12.43 12.43 0 0117.88-16.4l-2.32.18zM29 30.8A12.43 12.43 0 0032 14.95a.87.87 0 011.52-.87 14.18 14.18 0 01-20.89 18.34l-.07 2.66a.87.87 0 11-1.74-.04l.14-5.35 5.35-.1a.87.87 0 11.03 1.75l-2.16.04a12.43 12.43 0 0014.84-.58z" fill="#333"/><path d="M24.94 25.65a.9.9 0 00-.22-.57.75.75 0 00-.63-.3c-.17-.01-.32.03-.47.1-.6.43-1.29.65-2.05.65-.87 0-1.66-.22-2.35-.65a4.02 4.02 0 01-1.55-1.84h2.79c.25 0 .46-.07.63-.23a.77.77 0 00.25-.6.75.75 0 00-.25-.57.89.89 0 00-.63-.23l-3.2-.02-.04-.65v-.32l3.24.02c.25 0 .46-.08.63-.24a.77.77 0 00.25-.59.75.75 0 00-.25-.57.89.89 0 00-.63-.24l-2.9-.02a4.2 4.2 0 014.01-2.75c.74 0 1.42.21 2.05.63.16.09.32.13.49.13a.7.7 0 00.6-.29.93.93 0 00.2-.6.77.77 0 00-.42-.71c-.43-.28-.9-.49-1.4-.63-.5-.16-1-.24-1.52-.24a5.97 5.97 0 00-4.6 2.13 6.18 6.18 0 00-1.18 2.32h-.96a.89.89 0 00-.63.23.77.77 0 00-.25.6c0 .23.09.43.25.59.17.15.38.23.63.23h.69v.32l.01.63h-.7a.89.89 0 00-.63.24.77.77 0 00-.25.59c0 .24.09.44.25.6.17.15.38.23.63.23h1a5.85 5.85 0 008.68 3.32.79.79 0 00.3-.28.76.76 0 00.13-.42z" fill="#333"/></svg>  
            Chiffre d'affaire
          </a>
        </div>
        <form action="../../controler/dcnx.action.php" method="post" enctype="multipart/form-data">
          <a class="header_link"  >
            <button class="btn btn--logOut" name="deconnection"></button>
          </a>
        </form>
      </div>
    </header>
    <section class="page">
      

        <table class="table">

          <tbody>
            <tr class="white_row">
              <td><h3 class="page_title">Nouvel·le employé·e</h3></td>
            </tr>
            <form action="../../controler/addEmp.action.php" method="post" enctype="multipart/form-data">
              <tr class="white_row">
                <td>
                  <input class="input input--table " type='text' name ="nom" ref='nom' placeholder='nom' required="required"/>
                </td>
                <td>
                  <input class="input input--table " type='text' name = "prenom" ref='prenom' placeholder='prénom' required="required"/>
                </td>
                <td>
                  <select class="input input--table " ref='role' name="role" required="required">
                    <option value="" hidden>rôle</option>
                    <option value="apprenti">apprenti</option>
                    <option value="senior">senior</option>
                    <option value="expert">expert</option>
                  </select>
                </td>
                <td>
                  <input class="input input--table " type='text' ref='username' name="loginNewEmp" placeholder="nom d'utilisateur" required="required"/>
                </td>
                <td>
                  <input class="input input--table " type='password' ref='password' name="passNewEmp" placeholder='mot de passe' required="required"/>
                </td>
                <td class="text_right">
                  <button class="btn btn--addRow" name="addEmp">
                    <svg viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <circle cx="17.58" cy="17.5" r="17.5" fill="#3E81EA" />
                      <path d="M17.58 12.33v10.34M12.58 17.67h10" stroke="#fff" stroke-width="1.5" stroke-linecap="round" />
                    </svg>
                  </button>
                </td>
              </tr>
            </form>
          </tbody>

          <tbody class="currentTable">
            <tr class="white_row">
              <td colspan='5'>
                <h3 class="page_title">Liste du personnel</h3>
              </td>
              <td>
                <input class="input input--search" type="text" id="currentInput" onkeyup="searchFilter('currentEmp','currentInput', 'currentTable')" placeholder="Rechercher un·e employé·e..">
              </td>
            </tr>
            <tr>
              <th> Nom </th>
              <th> Prénom </th>
              <th> Rôle </th>
              <th> Nom d'utilisateur </th>
              <th></th>
              <th></th>
            </tr>
            <form>
              <?php for ($i=0; $i < count($emp); $i++) { ?>
                <tr class="currentEmp" id="delete<?php echo $emp[$i]['numEmp'];?>">
                  <td class="employe"><?php echo $emp[$i]['nom']; ?></td>
                  <td class="employe"><?php echo $emp[$i]['prenom']; ?></td>
                  <td>
                    <select class="input input--margin input--table selectRole role" ref='role' name="role" disabled>
                      <option value=""hidden><?php echo $emp[$i]['role']; ?></option>
                      <option  value="apprenti">apprenti</option>
                      <option value="senior">senior</option>
                      <option value="expert">expert</option>
                    </select>
                  </td>
                  <td><?php echo $emp[$i]['login']; ?></td>
                  <td></td>
                  <td class="text_right">
                    <button type="button" onclick="editButton(<?php echo $emp[$i]['numEmp'];?>)" class="btn btn--update updateEmp buttonEdit">
                      <svg viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.54 2.5c-1.4-1.4-2.42-1.61-2.9-1.61-.13 0-.21.01-.25.02l-.15.04-9.78 9.77-1.16 6 6.16-.99 9.78-9.78.04-.15c.04-.21.2-1.37-1.74-3.3zM1.89 14.11l.54-2.74c.32.06 1 .32 1.95 1.27 1.05 1.05 1.34 1.76 1.42 2.1l-2.83.45-1.08-1.08zm4.36-.9l6.04-6.05c.25.42.33.71.35.86L6.68 14a4.8 4.8 0 00-.43-.79zm-.67-.86a10.83 10.83 0 00-.75-.76l6.06-6.07a7.85 7.85 0 01.75.77l-6.06 6.06zm-1.6-1.43c-.28-.2-.55-.34-.78-.43l5.96-5.96c.15.02.45.1.87.34l-6.05 6.05zm9.52-3.75a6.88 6.88 0 00-1.54-2.1 6.16 6.16 0 00-1.94-1.4l1.71-1.72c.27.04.99.23 2.05 1.3 1.17 1.16 1.4 1.91 1.44 2.2L13.5 7.18z" fill="#3E81EA"/>
                      </svg>
                    </button>
                    <button type="button" class="btn btn--complete updateEmp" name="updateEmp" onclick="updateEmploye(<?php echo $emp[$i]['numEmp'];?>)">
                      <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19.72 7l-10 10M4.62 11.89l5.1 5.1" stroke-width="2" stroke-linecap="round" />
                      </svg>
                    </button>
                    <button type="button" class="btn btn--remove deleteEmp" onclick="deleteEmploye(<?php echo $emp[$i]['numEmp'];?>)">
                      <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18 6L6 18M6 6l11.6 11.6" stroke-width="2" stroke-linecap="round" />
                      </svg>
                    </button>
                  </td>
                </tr>
              <?php } ?>
            </form>
            <!--<tr class="white_row>
              <td colspan='6'><h3 class="page_title">Modification du rôle d'un·e employé·e</h3></td>
            </tr>
            <tr>
              <form action="../../controler/deleteEmp.action.php" method="post" enctype="multipart/form-data">
                <td></td>
                <td></td>
                <td>
                  <select class="input input--table" ref='role' name="red">
                    <option value="apprenti">apprenti</option>
                    <option value="senior">senior</option>
                    <option value="expert">expert</option>
                  </select>
                </td>
                <td>
                  <input class="input input--table" type='text' ref='username' name="newlogin" placeholder="nom d'utilisateur" required="required"/>
                </td>
                <td></td>
                <td>
                  <button class="btn btn--addRow" name="updateEmp">
                    <svg viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <circle cx="17.58" cy="17.5" r="17.5" fill="#3E81EA" />
                      <path d="M17.58 12.33v10.34M12.58 17.67h10" stroke="#fff" stroke-width="1.5" stroke-linecap="round" />
                    </svg>
                  </button>
                </td>
              </form>
            </tr>-->
          </tbody>

        </table>

    </section>
    <script src="../javascript/toggleMenuNav.js"></script>
    <script src="../javascript/editButton.js"></script>
    <script src="../javascript/searchFilter.js"></script>
    <script src="../javascript/deleteRow.js"></script>
  </body>
</html>
