function deleteEmploye(id){
  if(confirm('Êtes-vous sûr de vouloir supprimer cet élément ?')){
    $.ajax({
      type:'post',
      url:'../../controler/deleteEmp.action.php',
      data: {
        delete_id: id
      },
      success:function(data){
        $('#delete'+id).hide();
      }
    });
  }
}

function deleteOp(id){  
  if(confirm('Êtes-vous certain de vouloir finir cette opération ?')){
    $.ajax({
      type:'post',
      url:'../../controler/deleteOp.action.php',
      data: {
        delete: id
      },
      success:function(data){
        /* from result create a string of data and append to the div */
       
         $.each(data, function( key, value ) { 
           
           string = "<td class='operation'>"+value['description'] + "</td><td>"+value['type']+"</td><td> "+value['client']+"</td><td> "+value['assigne']+"</td><td class='finished'> "+value['statut']+"</td><td></td>";
         }); 
            $('#show_data_op_finish').html(string);
            $('#delete'+id).hide();
      }
    });
  }
}