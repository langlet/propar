<?php

    class Personne
    {
        protected $nom;//nom type string
        protected $prenom;//prenom type string

        public function __construct($nom,$prenom){
            $this->nom = $nom;
            $this->prenom=$prenom;
        }

        /**
         * Get the value of nom
         */ 
        public function getNom()
        {
                return $this->nom;
        }

        /**
         * Get the value of prenom
         */ 
        public function getPrenom()
        {
                return $this->prenom;
        }
    }
    

?>