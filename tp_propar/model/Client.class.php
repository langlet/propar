<?php

    require_once "DBconnection.class.php";

    class Client
    {
        private  $_numClient;
        private static $_cpt;
        

        public function __construct($nom, $prenom){
            $this->nom = $nom;
            $this->prenom = $prenom;
            $this->_numClient=++self::$_cpt;

        }

        //fonction qui affiche la liste des clients
        public static function listClient(){
            $db = DBconnection::getInstance()->getConnection();
            $list = $db->query('SELECT nom, prenom FROM client');
            return $list->fetchall(PDO::FETCH_ASSOC);
        }
    }
    
?>