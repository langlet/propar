<?php
    require_once "DBconnection.class.php";
    require_once "Personne.class.php";

    class Employe extends Personne
    {

        private $_numEmp;//ID type INT
        private $_role;//role de l'employe type string
        private $_login;//login type string
        private $_mdp;//mot de passe type string
        private $_db;//connection base de donnée

        public function __construct($nom, $prenom, $role, $login, $mdp){
            //je me connecte à la base de donnée
            $this->nom = $nom;
            $this->prenom = $prenom;
            $this->_role = $role;
            $this->_login = $login;
            $this->_mdp = $mdp;
            $this->_db = DBconnection::getInstance()->getConnection();
        } 
        //fonction qui permettra d'ajouter du personnel
        public function addEmp(){
            //pour ajouter du personnel, j'effectue une requête INSERT INTO à ma bdd avec les données qui vont être recupéré via $_POST
            $this->_db->query('INSERT INTO employe (nom, prenom, role, login, mdp) VALUES (UPPER("'.$this->nom.'"), UPPER("'.$this->prenom.'"), "'.$this->_role.'", "'.$this->_login.'", "'.md5($this->_mdp).'") ');

            
        }
        //fonction qui permettra de modifier le personnel 
        public static function updateEmp($numEmp, $role){
            $db = DBconnection::getInstance()->getConnection();
           $db->query("UPDATE employe SET role = '$role' WHERE numEmp = '$numEmp'");
        }
        //fonction qui permettra de supprimer du personnel
        public static function deleteEmp($id){
            $db = DBconnection::getInstance()->getConnection();
            $db->query("DELETE  FROM employe WHERE numEmp = '$id' "); 
        }
        //fonction qui permettra de calculer le CA
        public static function chiffreAffaire(){
            $db = DBconnection::getInstance()->getConnection();
            $cA =  $db->query('CALL calculer_CA')->fetch(PDO::FETCH_COLUMN);
            return $cA;
            
        }
        //fonction qui verifie la connection
        public static function verifConn($login, $pass){
            $db = DBconnection::getInstance()->getConnection();
            $results = $db->query('SELECT COUNT(*) FROM employe WHERE login = "'.$login.'" AND mdp = "'.$pass.'" ')->fetch(PDO::FETCH_COLUMN);
           
           return $results;
            
        }
        //function qui me recupere le rôle, le nom et le prenom de la personne qui se connecte
        public static function access($login, $pass){
            $db = DBconnection::getInstance()->getConnection();
            $access = $db->query('SELECT employe.role, nom , prenom FROM employe WHERE login = "'.$login.'" AND mdp = "'.$pass.'" ')->fetchall(PDO::FETCH_ASSOC);
            return $access;
            
        }

        
        //fonction qui affiche la liste des employes
        public static function listEmp(){
            $db = DBconnection::getInstance()->getConnection();
            $list = $db->query('SELECT numEmp, upper(nom) as nom, upper(prenom) as prenom ,role,login,mdp FROM employe');
            return $list->fetchall(PDO::FETCH_ASSOC);
        }

        public static function verifExist($login){
            $db = DBconnection::getInstance()->getConnection();
            $results = $db->query('SELECT COUNT(*) FROM employe WHERE login = "'.$login.'" ')->fetch(PDO::FETCH_COLUMN);
           
           return $results;
            
        }
        
    }

?>