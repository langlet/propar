<?php
    require_once "DBconnection.class.php";
    //require "client.class.php";

    class Operations 
    {
        private $_numOp;//id type int
        private $_type;//le type d'operations type string
        private $_statut;//l'avancé de l'operation type string
        private $_cout;//la valeur de l'operation type float
        private $_description;//detail de l'operation type string
        private $_db;//connection base de donnée

        public function __construct($type, $statut, $description){
            $this->_type = $type;
            $this->_statut = $statut;
            $this->_description = $description;
            $this->_db =DBconnection::getInstance()->getConnection();

        }

        //fonction qui permettra d'ajouter une operation
        public function addOp($emp, $client,$type){
            $numEmp = $this->_db->query('SELECT numEmp FROM employe WHERE nom = "'.$emp.'" ');
            $num = $numEmp->fetchColumn();
            $numClient = $this->_db->query('SELECT numClient FROM client WHERE nom = "'.$client.'" ');
            $numC = $numClient->fetchColumn();

            switch ($type) {  
                case 'petite':
                    $cout = 1000;
                    break;
                case 'moyenne':
                    $cout = 2500;
                    break;
                case 'grosse':
                    $cout = 10000;
                    break;
            }
            //pour ajouter une operation, j'effectue une requête INSERT INTO à ma bdd avec les données qui vont être recupéré via $_POST
            $this->_db->query('INSERT INTO operations (type, statut, cout, description, numEmp, numClient)  VALUES ("'.$this->_type.'", "'.$this->_statut.'", '.$cout.', "'.$this->_description.'", '.$num.' , '.$numC.' )');
            
        }
        
        //fonction qui permettra de supprimer une operation
        public static function deleteOp($id){
            $db = DBconnection::getInstance()->getConnection();
            $db->query("DELETE  FROM operations WHERE numOp = '$id' "); 
        }
        //fonction qui affiche la liste des operations
        public static function listOp($login){
            $db = DBconnection::getInstance()->getConnection();
            $list = $db->query('SELECT operations.numOp, operations.type,operations.statut,operations.description, concat(employe.nom ," ",employe.prenom) as assigne, concat(client.nom ," ", client.prenom) as client FROM operations ,employe, client WHERE operations.numEmp = employe.numEmp AND operations.numClient = client.numClient AND login = "'.$login.'" ');
            return $list->fetchall(PDO::FETCH_ASSOC);
        }
        //fonction qui affiche la liste des operations finies
        public static function listOpFinish($login){
            $db = DBconnection::getInstance()->getConnection();
            $list = $db->query('SELECT operations_backup.type,operations_backup.statut,operations_backup.description, concat(employe.nom ," ",employe.prenom) as assigne, concat(client.nom ," ", client.prenom) as client FROM operations_backup ,employe, client WHERE operations_backup.numEmp = employe.numEmp AND operations_backup.numClient = client.numClient AND login = "'.$login.'" ');
            return $list->fetchall(PDO::FETCH_ASSOC);
        }
        //fonction qui compte le nombre d'operation en cour suivant le rôle de l'employe
        public static function countOp($role){
            $db = DBconnection::getInstance()->getConnection();
            $count = $db->query('SELECT count("operations.*") from operations INNER JOIN employe ON operations.numEmp = employe.numEmp WHERE employe.role = "'.$role.'" ');
            return $count->fetch(PDO::FETCH_COLUMN);
        }

        public static function list(){
            $db = DBconnection::getInstance()->getConnection();
            $list = $db->query('SELECT operations.numOp, operations.type,operations.statut,operations.description, concat(employe.nom ," ",employe.prenom) as assigne, concat(client.nom ," ", client.prenom) as client FROM operations ,employe, client WHERE operations.numEmp = employe.numEmp AND operations.numClient = client.numClient ');
            return $list->fetchall(PDO::FETCH_ASSOC);
        }

        public static function listFinish(){
            $db = DBconnection::getInstance()->getConnection();
            $list = $db->query('SELECT operations_backup.type,operations_backup.statut,operations_backup.description, concat(employe.nom ," ",employe.prenom) as assigne, concat(client.nom ," ", client.prenom) as client FROM operations_backup ,employe, client WHERE operations_backup.numEmp = employe.numEmp AND operations_backup.numClient = client.numClient ');
            return $list->fetchall(PDO::FETCH_ASSOC);
        }

    }

    
?>