# PROPAR

## UML	
"tp_propar/propar.zargo"

## MAQUETTE	
"tp_propar/TP_Propar.pdf"
Maquette réalisée sur figma.
Le HTML a été fait à la main sans utilisation de librairie ou de thème. Et est responsive pour la plupart des pages (sauf celles qui ont du changer plusieurs fois en cours de route suite à nos modifs ajax / back)

## MCD
"tp_propar/propar"	

## MLD	
"tp_propar/propar.mld"

## POO

## MVC
"tp_propar/propar"

## GIT	
https://bitbucket.org/langlet/propar/src/master/ 

## LOG4PHP
"tp_propar/controler/cnx.action.php" : Une info est générée dans le fichier myLog.log si les identifiants renseignés sont inconnus

## BDD	
SQL : "tp_propar/propar.sql" : 
Nom d'utilisateur : admin > mot de passe : 1234 (tout les MDP sont les meme pour chaques users)

## TRIGGER	
SQL : "tp_propar/propar.sql"

## PROCEDURE STOCKEE
SQL : "tp_propar/propar.sql"

## FICHIER	
Log4PHP : "tp_propar/logging/myLog.log"

## HTACCESS	
Gestion d'erreur 404

## D PATTERN	
MVC

## TRELLO	
https://trello.com/invite/b/1f8MGWsN/1ba665fd7ef264604498679f403ebe89/tppropar

## AJAX	

"tp_propar/view/javascript/afficheOp.js" : 
Affiche sur la view "connection.php" les données de la DB avec les controlers "listOp.action.php" et "listOpFinish.action.php"

"tp_propar/view/javascript/deleteRow.js" :
Masque la ligne qui contient des données provenant de la DB sur la view "operation_admin.php" / "operation.php" et "employee.php" avec les controlers "deleteEmp.action.php" et "deleteOp.action.php"

"tp_propar/view/javascript/editButton.js" :
Modifie le rôle de l'employe "employee.php" avec les controlers "deleteEmp.action.php"

## JS

"tp_propar/view/javascript/editButton.js" : 
Rend le select enabled ou disabled au clic du bouton edit 

"tp_propar/view/javascript/searchFilter.js" :
Barre de recherche : Rechercher une donnée dans une colonne d'un tableau en fonction d'une saisie dans l'input. 

"tp_propar/view/javascript/toggleMenuNav.js" :
Navigation pour le header: Active / desactive un menu deroulant au clic d'un bouton


## SRC